Ściema is for shading
============

This is a clone of the popular ShaderToy, except it all runs offline.

Running
----------

You need Python3 and PyQt5. Start the program with:

```
./Ściema.py
```

Your shader will be autosaved to the current directory as `shader.stoy`.

Examples
------------

Check out the examples in the `examples` directory.

![Window with the shader and code](doc/Ściema.png)
`spheres.stoy` example in action.

License
----------

Code released under the AGPL-3.0 license, or later, at your leisure.
