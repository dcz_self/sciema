/*
 Based on https://github.com/electricsquare/raymarching-workshop
 MIT License
 Copyright (c) 2018 Electric Square Ltd
 
 SDF transformations based on https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 
 Remaining pieces by Dorota Czaplejewicz <gihuac.dcz@porcupinefactory.org>
 SPDX-License-Identifier: AGPL-3.0-or-later
 */

const int NOTHING = -1;
const int MIRROR = 0;
const int SPHERE = 1;
const int LIGHT = 2;
const int DARK = 3;

vec3 lightdir = normalize(vec3(1.0, 1.0, -1.0));
vec3 lightcolor = vec3(1.0, .7, 0.2);

struct rayinfo {
   float dist;
   int material;
};

mat4 rotateZmat(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4(c, s, 0, 0),
        vec4(-s, c, 0, 0),
        vec4(0, 0, 1, 0),
        vec4(0, 0, 0, 1)
    );
}

vec3 rotateZ(vec3 pos, float a) {
    return (rotateZmat(a) * vec4(pos, 1.0)).xyz;
}


mat4 rotateXmat(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4(1, 0, 0, 0),
        vec4(0, c, -s, 0),
        vec4(0, s, c, 0),
        vec4(0, 0, 0, 1)
    );
}

vec3 rotateX(vec3 pos, float a) {
    return (rotateXmat(a) * vec4(pos, 1.0)).xyz;
}


mat4 rotateYmat(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4(c, 0, s, 0),
        vec4(0, 1, 0, 0),
        vec4(-s, 0, c, 0),
        vec4(0, 0, 0, 1)
    );
}

vec3 rotateY(vec3 pos, float a) {
    return (rotateYmat(a) * vec4(pos, 1.0)).xyz;
}

float sdTorus( vec3 p, vec2 t )
{
  vec2 q = vec2(length(p.xz)-t.x,p.y);
  return length(q)-t.y;
}

float sdSphere(vec3 p, float r)
{
    return length(p)-r;
}

float sdBox( vec3 p, vec3 b )
{
  vec3 q = abs(p) - b;
  return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
}

float opSmoothSubtraction( float d1, float d2, float k ) {
    float h = clamp( 0.5 - 0.5*(d2+d1)/k, 0.0, 1.0 );
    return mix( d2, -d1, h ) + k*h*(1.0-h);
}

float opSmoothUnion( float d1, float d2, float k ) {
    float h = clamp( 0.5 + 0.5*(d2-d1)/k, 0.0, 1.0 );
    return mix( d2, d1, h ) - k*h*(1.0-h);
}

rayinfo fat_union(rayinfo a, rayinfo b) {
  if (min(a.dist, b.dist) == a.dist) {
     return a;
  } else {
     return b;
  }
}

float spheres(vec3 pos)
{
    float t = sdSphere(pos-vec3(0,0,10), 3.0);
    t = min(t, sdSphere(pos - vec3(5, 0, 13), 2.0));
    t = opSmoothSubtraction(
      sdSphere(pos - vec3(-0.2, -0.5, 6.70), 0.95), 
      t, 0.3);
    t = opSmoothUnion(t,
     sdSphere(pos - vec3(1.0, 3.0, 8.0), 0.5),
     0.5);
    t = opSmoothUnion(t,
     sdSphere(pos - vec3(-2.6, -0.4, 9.0), 0.5),
     0.5);
    t = opSmoothUnion(t,
     sdSphere(pos - vec3(-2.0, -2.0, 8.0), 0.5),
     0.5);
    t = opSmoothUnion(t,
       sdSphere(pos - vec3(-5, -4.3, 11.0), 1.0),
     0.5);
    return t;
}

rayinfo fat_spheres(vec3 pos) {
   rayinfo r;
    r.dist = spheres(pos);
   r.material = SPHERE;
   return r;
}

rayinfo fat_light(vec3 pos) {
  rayinfo r;
   vec3 py = rotateZ(pos - vec3(3, 2.0, 7.0), 0.5);
  r.dist = sdTorus(py,
    vec2(1, 0.1));
  r.material = LIGHT;
  return r;
}

rayinfo fat_dark(vec3 pos) {
   rayinfo r;
    r.dist = sdBox(rotateZ(pos - vec3(-.5, 2.3, 8.0), 0.40), vec3(1, .1, 0.1));
   r.material = DARK;
   return r;
}

rayinfo fat_mirror(vec3 pos) {
  rayinfo r;
  r.dist = sdBox(
      rotateZ(
       rotateY(pos - vec3(-7.0, 7., 20.0), 1.1),
      0.5),
      vec3(.1, 4.0, 4.0));
   r.material = MIRROR;
   return r;
}

rayinfo fat_sdf(vec3 pos) {
   rayinfo r = fat_spheres(pos);
   r = fat_union(fat_dark(pos), r);
   r = fat_union(fat_mirror(pos), r);
   return fat_union(r, fat_light(pos));
}

float sdf(vec3 pos) {
  return fat_sdf(pos).dist;
}

vec3 calcNormal(vec3 pos)
{
    // Center sample
    float c = sdf(pos);
    // Use offset samples to compute gradient / normal
    vec2 eps_zero = vec2(0.001, 0.0);
    return normalize(vec3( sdf(pos + eps_zero.xyy), sdf(pos + eps_zero.yxy), sdf(pos + eps_zero.yyx) ) - c);
}

rayinfo castFatRay(vec3 rayOrigin, vec3 rayDir)
{
    float tmax = 20.0;
    rayinfo current;
    current.dist = 0.0; // Stores current distance along ray
    current.material = NOTHING;

    for (int i = 0; i < 256; i++)
    {
        rayinfo res = fat_sdf(rayOrigin + rayDir * current.dist);

        current.material = res.material;
        if (res.dist < (0.0001*current.dist))
        {
            return current;
        }
        else if (res.dist > tmax)
        {
            current.dist = -1.0;
            current.material = NOTHING;
            return current;
        }
        current.dist += res.dist;
    }
    current.dist = -1.0;
    current.material = NOTHING;
    return current;
}


float fresnel(vec3 incident, vec3 normal, float n0, float n1) {
// Schlick's approximation following Wikipedia
    float cosphi = -dot(incident, normal);
    float cosphi1 = 1.0 - cosphi;
    float cosphi2 = cosphi1 * cosphi1;
    float cosphi5 = cosphi2 * cosphi2 * cosphi1;
    float sqr0 = (n0 - n1) / (n0 + n1);
    float r0 = sqr0 * sqr0;
    return r0 + (1.0 - r0) * cosphi5;
}

vec3 lampshadeRay(vec3 rayDir) {
    vec3 color = vec3(0.0);
    if (dot(rayDir, -lightdir) < -0.9) {
       color += lightcolor * 50.0 * -(dot(rayDir, -lightdir) + 0.9);
    }
return color;
}

vec3 hardRay(vec3 rayDir, float r) {
    vec3 color = vec3(0.0);
    if (dot(rayDir, -lightdir) < -r) {
       color += lightcolor * 2.0;
    }
return color;
}

vec3 castColorRay(vec3 rayOrigin, vec3 rayDir) {
  vec3 col = vec3(0.0);
  float contribution = 1.0;
  vec3 objcol = vec3(0.0);
    float shiny = 0.0;
 int max_reflections = 5;
  for (int i = 0; i < max_reflections; i++) {
   if (contribution < 0.05) { break; }
      rayinfo r = castFatRay(rayOrigin, rayDir);
      float t = r.dist;

   if (r.material == NOTHING) {
   col += contribution * hardRay(rayDir, 0.97);
    col += contribution * vec3(max(rayDir.y + 0.2, 0.0) * 0.7, 0.0, 0);
    return col;
  }
  if (r.material == LIGHT) {
    col += contribution * vec3(2.0);
    return col;
  }
  if (r.material == SPHERE) {
    objcol = vec3(1.0);
    shiny = 0.2;
  }
  if (r.material == DARK) {
    shiny = 0.2;
   objcol = vec3(0.0);
  }
  if (r.material == MIRROR) {
    shiny = 1.0;
  }
  // SPHERE and DARK
    vec3 pos = rayOrigin + rayDir * t;
    vec3 norm = calcNormal(pos);

    float light_contrib = 0.0;
      rayinfo shade = castFatRay(pos, lightdir);
      if (shade.dist > 0.0 && shade.material != LIGHT) {
          light_contrib = 0.0;
      } else {
        light_contrib = max(dot(norm, lightdir), 0.0);
    }
    float reflectance = shiny + (fresnel(rayDir, norm, 1.0, 1.5) * (1.0 - shiny));
;
    float absorption = 1.0 - reflectance;

    vec3 diffuse = vec3(0.0);
    diffuse += lightcolor * light_contrib * objcol;

    vec3 ambientcolor = vec3(0.01, 0.02, 0.01);
    col += contribution * absorption * (diffuse + ambientcolor);

     contribution *= reflectance;

    rayOrigin = pos;
    rayDir = reflect(rayDir, norm);

  }
  return col;
}


vec3 castReflection(vec3 rayOrigin, vec3 rayDir) {
  vec3 color = hardRay(rayDir, 0.95);
  color += castColorRay(rayOrigin, rayDir);
  return color;
}

vec3 render(vec3 rayOrigin, vec3 rayDir)
{
  vec3 col = castColorRay(rayOrigin, rayDir);
col = pow(col, vec3(0.4545)); // Gamma correction
    return col;
}

vec3 getCameraRayDir(vec2 uv, vec3 camPos, vec3 camTarget)
{
	vec3 camForward = normalize(camTarget - camPos);
	vec3 camRight = normalize(cross(vec3(0.0, 1.0, 0.0), camForward));
	vec3 camUp = normalize(cross(camForward, camRight));
							  
    float fPersp = 2.0;
	vec3 vDir = normalize(uv.x * camRight + uv.y * camUp + camForward * fPersp);

	return vDir;
}

vec2 normalizeScreenCoords(vec2 screenCoord)
{
    vec2 result = 2.0 * (screenCoord/iResolution.xy - 0.5);
    result.x *= iResolution.x/iResolution.y;
    return result;
}

void mainImage(out vec4 fragColor, vec2 fragCoord)
{
    vec3 camPos = vec3(0, 0, -1);
    vec3 at = vec3(0, 0, 0);
    
    vec2 uv = normalizeScreenCoords(fragCoord);
    vec3 rayDir = getCameraRayDir(uv, camPos, at);   
    
    vec3 col = render(camPos, rayDir);
    
    fragColor = vec4(col,1.0); // Output to screen
}
