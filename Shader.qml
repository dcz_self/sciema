/*
 Author: Dorota Czaplejewicz <gihuac.dcz@porcupinefactory.org>
 SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.0

ShaderEffect {
    property string prelude: "uniform lowp float qt_Opacity;
varying highp vec2 qt_TexCoord0;
uniform vec3 iResolution;
uniform float iTime;
"
    property string code

    property string main: "
void main(void) {
    // Qt draws on the entire window by default, and gives qt_TexCoord0 in exchange.
    // That is, however, top-down.
    vec2 vertflip = vec2(qt_TexCoord0.x, 1.0 - qt_TexCoord0.y);
    mainImage(gl_FragColor, vertflip * iResolution.xy);
}"
    fragmentShader: prelude + code + main;

    readonly property vector3d iResolution: Qt.vector3d(width, height, 0.0)
    property real iTime: 0.0
}
