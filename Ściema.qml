/*
 Author: Dorota Czaplejewicz <gihuac.dcz@porcupinefactory.org>
 SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick.Controls 1.3
import QtQuick 2.4
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.0
import Utils 0.1

ApplicationWindow {
    visibility: "Maximized"
    visible: true
    title: qsTr("Ściema")
    width: 640
    height: 360

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        selectExisting: false
        onAccepted: {
            file_source.path = fileDialog.fileUrl;
            fileDialog.close()
        }
    }

    SplitView {
        id: splitView1
        anchors.fill: parent

        SplitView {
            id: splitView
            orientation: Qt.Vertical
            Layout.fillWidth: true
            //layer.enabled: true
            Shader {
                id: viewport
                code: _source.text
                Layout.fillHeight: true
            }

            TextArea {
                text: viewport.log
                readOnly: true

                height: 200
                Layout.fillHeight: true
            }
        }

        ColumnLayout {
            Layout.fillWidth: true
            width: 500

            File {
                id: file_source
                path: ""
                onPathChanged: {
                    var contents = file_source.read()
                    if (contents) {
                        _source.text = contents;
                    }
                }
            }

            RowLayout {
                Label {
                    text: file_source.path || "Unsaved"
                    Layout.fillWidth: true
                }
                Button {
                    text: "Change"
                    onClicked: {
                        fileDialog.open()
                    }
                }
            }

            TextArea {
                Layout.fillWidth: true
                Layout.fillHeight: true
                id: _source
                text: "void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    fragColor = vec4(0.5, 0.5, 1.0, 1.0);
}
"
                onTextChanged: {
                    if (file_source.path) {
                        file_source.write(text);
                    }
                }
            }

            Label {
                Layout.fillHeight: false
                text: {
                    if (viewport.status == ShaderEffect.Compiled) {
                        "OK"
                    } else if (viewport.status == ShaderEffect.Uncompiled) {
                        "Going…"
                    } else if (viewport.status == ShaderEffect.Error) {
                        "ERROR"
                    } else {
                        "?"
                    }
                }
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
        }
    }
}


