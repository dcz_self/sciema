#! /usr/bin/env python3
import sys

from PyQt5.QtCore import pyqtProperty, pyqtSignal, pyqtSlot, QObject, QUrl
from PyQt5.QtWidgets import QApplication
from PyQt5.QtQml import qmlRegisterType, QQmlComponent, QQmlEngine


class File(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.filepath = None
 
    onPathChanged = pyqtSignal()

    @pyqtProperty('QString', notify=onPathChanged)
    def path(self):
        return self.filepath

    @path.setter
    def path(self, path):
        if path.startswith('file://'):
            path = path[len('file://'):]
        self.filepath = path
        self.onPathChanged.emit()

    @pyqtSlot(result='QString')
    def read(self):
        try:
            with open(self.filepath) as f:
                return f.read()
        except:
            print("Failed to read", self.filepath)
            return

    @pyqtSlot(str)
    def write(self, data):
        with open(self.filepath, 'w') as f:
            f.write(data)

app = QApplication(sys.argv)
qmlRegisterType(File, 'Utils', 0, 1, 'File')

engine = QQmlEngine()
component = QQmlComponent(engine)
component.loadUrl(QUrl('Ściema.qml'))

ściema = component.create()
if not ściema:
    sys.exit(1)
sys.exit(app.exec())
